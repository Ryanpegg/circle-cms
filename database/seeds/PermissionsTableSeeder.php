<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('permissions')->insert([
            [
              'id' => 1,
              'name' => 'manage_all_users',
              'label' => 'Manage all users'
            ], [
              'id' => 2,
              'name' => 'create_admin',
              'label' => 'Create Admins'
            ], [
              'id' => 3,
              'name' => 'create_author',
              'label' => 'Create Authors'
            ], [
              'id' => 4,
              'name' => 'create_editor',
              'label' => 'Create Editors'
            ], [
              'id' => 5,
              'name' => 'create_user',
              'label' => 'Create Users'
            ], [
              'id' => 6,
              'name' => 'use_circle',
              'label' => 'Use Circle CMS'
            ],
        ]);
    }
}
