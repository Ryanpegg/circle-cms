<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('roles')->insert([
            [
              'id' => 1,
              'name' => 'admin',
              'label' => 'Admin'
            ], [
              'id' => 2,
              'name' => 'author',
              'label' => 'Author'
            ], [
              'id' => 3,
              'name' => 'editor',
              'label' => 'Editor'
            ], [
              'id' => 4,
              'name' => 'user',
              'label' => 'User'
            ],
        ]);
    }
}
