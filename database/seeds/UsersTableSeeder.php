<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
            [
              'id' => 1,
              'name' => "Admin",
              'email' => 'admin@circle-cms.com',
              'password' => bcrypt('password'),
              'remember_token' => str_random(10),
            ],
        ]);
    }
}
