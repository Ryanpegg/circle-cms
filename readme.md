# CIRCLE CMS

A Laravel based project that will be used for future developments and allow those involved to easily create websites for clients.

## Setting Up

First clone the project
```bash
  git clone https://#YOUR_USERNAME#@bitbucket.org/Ryanpegg/circle-cms.git
```

Then update the project files to install the rest of the project.

```bash
  composer update
```

Then the .env file will need to be recreated by the following command

```bash
  cp .env.example .env
  php artisan key:generate
```

Then make sure the database settings have been set in the newly created .env file. Once the database has been created and the setting have been created the database will need to be populated. Run the commands to set up the tables and seed the data.

```bash
  php artisan migrate
  php artisan db:seed
```

Finally, run npm install to include all of the packages being used.

```bash
  npm install
```

## Development Procedures

All new features will be built on a new branch by using the command below. When the feature has been completed a pull request should be created explaining what feature has been built. The branch will then be merged into the master branch.

```bash
  git checkout -b new_feature_name
```

Whenever a new branch has been merged into the master or a branch that you are workng from it will need to be rebased. In order to do this you will have to fetch all updates and then use the rebase command to rebase. Like so...
```bash
  git fetch origin
  git rebase origin/#THE_BRANCH_YOUR_WORKING_FROM#
```
! Between the ## is the branch you are working from (the branch you got the created the branch from)

### Branch Creation

Branches will be created for each feature and only one person should be working from one branch. The naming scheme is to be something reletive with any words seperated by underscores. For example,

```bash
  git checkout -b new_feature_branch
```

## Style

The style will be controlled by the SCSS style sheets which will be rendered when requested. Variables will be stored in a seperate file which will be included right at the top. Fonts will also be stored in this file.

Large sections of style will be seperated into different files then included into the main file. This will make for better and easier editing.

To compile the style there are a range of commands that can be used to compile the style sheets

```bash
  npm run dev (Will compile when requested)
  npm run watch (Will compile in dev when ever a file concerned is edited)
  npm run production (Will compile in a minified state best for release)
```

## JavaScript

This project will use JQuery to allow users to interact better and certain features to run smoother. New JS files will be added to the directory "/resources/assets/js/circle-cms/". They will then need to be added to the webpack.mix file which can be found in the route directory.

The file can be added to the webpack.mx file by adding the a line to the exixting list of lines, like below.

```javascript
  ...
  .js('resources/assets/js/circle-cms/#FILENAME#.js', 'public/js/circle-cms')
  ...
```

After adding the file if you are running watch(npm run watch) they you will have to restart the command.