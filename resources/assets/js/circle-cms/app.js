function removeMessage(){
  $('.message').animate({
    'right': '-100%'
  }, 500, function(){
    $(this).remove();
  })
}

$(document).ready(function(){
  $('.message').addClass('hiding');

  setTimeout(function(){
    removeMessage();
  }, 10000)

  $('.message .remove').on('click', function(){
    removeMessage();
  })

  $('.close-panel').on('click', function(e){
    e.preventDefault();
    $('.side-panel').removeClass('active');
    $('.cover').removeClass('active');
  })
  $('.cover').on('click', function(){
    $('.side-panel').removeClass('active');
    $('.cover').removeClass('active');
  })
  $('.hotbar A').on('click', function(e){
    e.preventDefault();
    var target = $(this).attr('href');
    target = $(target);
    target.parent().siblings('.panel-header').children('.title').children('SPAN').html(target.data('title'))
    $('.panels > DIV').removeClass('active');
    $('.side-panel').removeClass('active');
    target.addClass('active');
    target.parent().parent().addClass('active');
    $('.cover').addClass('active');
  })
  $('.menu-button').on('click', function(e){
    e.preventDefault();
    $(this).toggleClass('active');
    $('NAV').toggleClass('open');
  })
})
