@extends('layouts.circle-cms.logged_out')

@section('title', 'Login')

@section('content')
  <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
    @csrf
    {!! Form::label('email') !!}
    {!! Form::email('email', old('email') ) !!}
    @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
    {!! Form::label('password') !!}
    {!! Form::password('password') !!}
    @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
    <label>
        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
    </label>
    <div class="buttons">
      {!! Form::submit('Login', ['class' => "ghost-button"]) !!}
      <a href="{{ route('password.request') }}">Forgot your password?</a>
    </div>
  </form>
@endsection
