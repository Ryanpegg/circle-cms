@extends('layouts.circle-cms.logged_out')

@section('title', 'Reset Password')

@section('content')
  <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
    <h3>Reset Password</h3>
    @csrf
    {!! Form::label('email') !!}
    {!! Form::email('email', old('email') ) !!}
    @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
    <div class="buttons">
      {!! Form::submit('Send Reset Email', ['class' => "ghost-button"]) !!}
      <a href="{{ route('login') }}"><i class="fas fa-chevron-left"></i> Login</a>
    </div>
  </form>
@endsection
