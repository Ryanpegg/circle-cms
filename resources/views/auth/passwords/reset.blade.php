@extends('layouts.circle-cms.logged_out')

@section('title', 'Reset Password')

@section('content')
  <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
    <h3>Reset Password</h3>
    @csrf
    <input type="hidden" name="token" value="{{ $token }}">
    {!! Form::label('email') !!}
    {!! Form::email('email', old('email') ) !!}
    @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
    {!! Form::label('password', 'New Password') !!}
    {!! Form::password('password') !!}
    @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
    {!! Form::label('password_confirmation') !!}
    {!! Form::password('password_confirmation') !!}
    <div class="buttons">
      {!! Form::submit('Reset Password', ['class' => "ghost-button"]) !!}
      <a href="{{ route('login') }}"><i class="fas fa-chevron-left"></i> Login</a>
    </div>
  </form>
@endsection
