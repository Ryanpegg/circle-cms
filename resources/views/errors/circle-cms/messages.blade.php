@if(Session::has('success'))
  <div class="message success">
    <div class="icon">
      <i class="far fa-check-circle"></i>
    </div>
    <div class="text">
      {{ Session::get('success') }}
    </div>
    <div class="remove">
      <i class="fas fa-times"></i>
    </div>
  </div>
@elseif(Session::has('warning'))
  <div class="message warning">
    <div class="icon">
      <i class="fas fa-exclamation-triangle"></i>
    </div>
    <div class="text">
      {{ Session::get('warning') }}
    </div>
    <div class="remove">
      <i class="fas fa-times"></i>
    </div>
  </div>
@elseif(Session::has('error'))
  <div class="message error">
    <div class="icon">
      <i class="far fa-times-circle"></i>
    </div>
    <div class="text">
      {{ Session::get('error') }}
    </div>
    <div class="remove">
      <i class="fas fa-times"></i>
    </div>
  </div>
@endif
