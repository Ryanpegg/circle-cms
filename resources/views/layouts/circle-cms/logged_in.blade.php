<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>CIRCLE-CMS|@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/circle-cms/logged_in.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="/js/circle-cms/app.js"></script>
  </head>
  <body>
    <header>
      <div class="hotbar">
        <a href="#search" class="search">
          <i class="fas fa-search"></i>
        </a>
        <a href="#notifications" class="notifications">
          <i class="far fa-bell"></i>
          <!-- <i class="fas fa-bell"></i> -->
        </a>
        <a href="#menu-button" class="menu-button">
          <i class="fas fa-bars"></i>
        </a>
        <a href="#user-settings" class="user-settings">
          <i class="fas fa-user-cog"></i>
        </a>
        <a href="#logout" class="logout">
          <i class="fas fa-power-off"></i>
        </a>
      </div>
    </header>
    <nav>
      <a href="/circle-cms/home" class="logo">
        <img src="{{ asset('images/logo/Circle-CMS-logo(dark).png') }}" alt="logo"/>
      </a>
      <ul class="pages">
        <li><a href="/circle-cms/home"><i class="fas fa-home"></i> Home</a></li>
        <li><a href="/circle-cms/home"><i class="far fa-file-alt"></i> Pages</a></li>
        <li><a href="/circle-cms/home"><i class="fas fa-users"></i> Users</a></li>
      </ul>
    </nav>
    <div class="side-panel left">
      <div class="panel-header">
        <a href="#" class="close-panel">
          <i class="fas fa-chevron-left left"></i>
          <i class="fas fa-chevron-right"></i>
        </a>
        <div class="title">
          <span>Title</span>
        </div>
      </div>
      <div class="panels">
        <div id="search" data-title="Site Search">
          <p>Search Box</p>
        </div>
        <div id="notifications" data-title="Notifications">
          <p>No New Notifications</p>
        </div>
      </div>
    </div>
    <div class="side-panel">
      <div class="panel-header">
        <a href="#" class="close-panel">
          <i class="fas fa-chevron-right"></i>
        </a>
        <div class="title">
          <span>Title</span>
        </div>
      </div>
      <div class="panels">
        <div id="user-settings" data-title="User Settings">
          <p>Edit your user settings</p>
        </div>
        <div id="logout" data-title="Logout">
          <p>Are you sure you want to logout?</p>
          <a href="{{ route('logout') }}" class="button" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Logout
          </a>
          <a href="#" class="close-panel button cancel">
            Cancel
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>
        </div>
      </div>
    </div>

    <div class="page">

      @yield('content')

    </div>
    @include('errors.circle-cms.messages')
    <div class="cover">

    </div>
  </body>
</html>
