<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>CIRCLE-CMS|@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/circle-cms/logged_out.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>
  <body>
    <div class="login">
      <div class="logo">
        <img src="{{ asset('images/logo/Circle-CMS-logo(dark).png') }}" alt="logo"/>
      </div>
      <div class="content">
        @yield('content')
      </div>
    </div>
  </body>
</html>
