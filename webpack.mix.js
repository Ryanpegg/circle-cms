let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/circle-cms/app.js', 'public/js/circle-cms')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/circle-cms/logged_out.scss', 'public/css/circle-cms/')
   .sass('resources/assets/sass/circle-cms/logged_in.scss', 'public/css/circle-cms/')
   .copy('resources/assets/images', 'public/images');
